/*
 * @author : neiko.neikov
 * @created : 4.7.2024 г., четвъртък
 */
package org.faac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
