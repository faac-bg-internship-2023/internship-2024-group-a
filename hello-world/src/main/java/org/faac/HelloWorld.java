/*
 * @author : neiko.neikov
 * @created : 4.7.2024 г., четвъртък
 */
package org.faac;

public class HelloWorld {
    public static void main(String[] args) throws Exception {
        System.in.read(new byte[] {});
        System.out.println("Hello World");
    }
}
